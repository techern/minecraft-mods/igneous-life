package org.techern.minecraft.igneouslife.items.tools;

import org.techern.minecraft.igneouslife.items.IgneousItem;

import java.util.List;

/**
 * An interface defining an {@link org.techern.minecraft.igneouslife.items.IgneousItem} tool as {@link net.minecraftforge.common.ToolType} does not include hoes
 *
 * @param <T> used for the getAll function
 * @since 0.8
 */
public interface IgneousTool<T extends IgneousTool> extends IgneousItem {

    /**
     * Gets the {@link String} defining the tool type
     *
     * @return a {@link String}, duh
     * @since 0.8
     */
    public String getToolType();

    /**
     * Gets a {@link List} of all of the {@link IgneousTool}s of this type
     *
     * @return a {@link List}
     * @since 0.8
     */
    public List<T> getAll();
}
