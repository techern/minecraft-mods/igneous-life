package org.techern.minecraft.igneouslife.items.tools;

import net.minecraft.block.Block;
import net.minecraft.item.*;
import org.techern.minecraft.igneouslife.items.IgneousItem;
import org.techern.minecraft.igneouslife.items.IgneousLifeItems;

import java.util.List;

/**
 * An extension of {@link PickaxeItem} that is actually an {@link IgneousItem}
 *
 * @since 0.8
 */
public class IgneousPickaxe extends PickaxeItem implements IgneousTool<IgneousPickaxe> {

    /**
     * The {@link Block} backing this {@link Item}
     *
     * @since 0.8
     */
    private Block block;

    /**
     * Gets the {@link Block} that this item is based on
     *
     * @return The {@link Block}
     * @since 0.8
     */
    public Block getBlock() {
        return block;
    }

    /**
     * Creates a new {@link IgneousPickaxe} with prefilled data
     *
     * @param blockType The {@link Block} that we are basing this {@link Item} on
     * @since 0.8
     */
    public IgneousPickaxe(Block blockType) {
        super(ItemTier.STONE, 1, -2.8F, (new Item.Properties()).group(ItemGroup.TOOLS));
        this.block = blockType;

        setRegistryName(blockType.getRegistryName().getPath() + "_pickaxe");
    }

    /**
     * Gets the {@link String} defining the tool type
     *
     * @return a {@link String}, duh
     * @since 0.8
     */
    @Override
    public String getToolType() {
        return "pickaxe";
    }

    /**
     * Gets a {@link List} of all of the {@link IgneousTool}s of this type
     *
     * @return a {@link List}
     * @since 0.8
     */
    @Override
    public List<IgneousPickaxe> getAll() {
        return IgneousLifeItems.getPickaxes();
    }
}
