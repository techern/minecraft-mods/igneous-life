package org.techern.minecraft.igneouslife.items;

import net.minecraft.item.Item;
import org.techern.minecraft.igneouslife.items.tools.IgneousAxe;
import org.techern.minecraft.igneouslife.items.tools.IgneousHoe;
import org.techern.minecraft.igneouslife.items.tools.IgneousPickaxe;
import org.techern.minecraft.igneouslife.items.tools.IgneousShovel;

import java.util.ArrayList;
import java.util.List;

/**
 * A utility class for holding all {@link org.techern.minecraft.igneouslife.IgneousLifeMod} items
 *
 * @since 0.8
 */
public class IgneousLifeItems {

    /**
     * An internal {@link List} of pickaxes
     *
     * @since 0.8
     */
    private static List<IgneousPickaxe> pickaxes = new ArrayList<>();

    /**
     * Gets a list of pickaxes
     *
     * @return The list of pickaxes
     * @since 0.8
     */
    public static List<IgneousPickaxe> getPickaxes() {
        return pickaxes;
    }

    /**
     * An internal {@link List} of axes
     *
     * @since 0.8
     */
    private static List<IgneousAxe> axes = new ArrayList<>();

    /**
     * Gets a list of axes
     *
     * @return The list of axes
     * @since 0.8
     */
    public static List<IgneousAxe> getAxes() {
        return axes;
    }

    /**
     * An internal {@link List} of shovels
     *
     * @since 0.8
     */
    private static List<IgneousShovel> shovels = new ArrayList<>();

    /**
     * Gets a list of shovels
     *
     * @return The list of shovels
     * @since 0.8
     */
    public static List<IgneousShovel> getShovels() {
        return shovels;
    }

    /**
     * An internal {@link List} of hoes
     *
     * @since 0.8
     */
    private static List<IgneousHoe> hoes = new ArrayList<>();

    /**
     * Gets a list of hoes
     *
     * @return The list of hoes
     * @since 0.8
     */
    public static List<IgneousHoe> getHoes() {
        return hoes;
    }

    /**
     * An internal {@link List} of swords
     *
     * @since 0.8
     */
    private static List<IgneousSword> swords = new ArrayList<>();

    /**
     * Gets a list of swords
     *
     * @return The list of swords
     * @since 0.8
     */
    public static List<IgneousSword> getSwords() {
        return swords;
    }

    /**
     * Gets all {@link Item}s added by this mod
     *
     * @return An {@link ArrayList} of all the items
     * @since 0.8
     */
    public static List<Item> getAllItems() {
        ArrayList<Item> items = new ArrayList<>(1000);

        items.addAll(getPickaxes());
        items.addAll(getAxes());
        items.addAll(getHoes());
        items.addAll(getShovels());

        items.addAll(getSwords());

        return items;
    }

}
