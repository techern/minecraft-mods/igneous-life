package org.techern.minecraft.igneouslife.items;

import net.minecraft.block.Block;
import net.minecraft.item.Item;
import net.minecraft.item.ItemGroup;
import net.minecraft.item.ItemTier;
import net.minecraft.item.SwordItem;

/**
 * An extension of the {@link SwordItem} that allows us to automatically generate data based on the internal {@link Block}
 *
 * @since 0.8
 */
public class IgneousSword extends SwordItem implements IgneousItem {

    /**
     * The {@link Block} that this {@link IgneousSword} is based on
     *
     * @since 0.8
     */
    private Block block;

    /**
     * Creates a new {@link IgneousSword} with stone-tier data
     *
     * @param block The {@link Block} that we are using
     * @since 0.8
     */
    public IgneousSword(Block block) {
        super(ItemTier.STONE, 3, -2.4F, (new Item.Properties()).group(ItemGroup.COMBAT));
        this.block = block;

        setRegistryName(block.getRegistryName().getPath() + "_sword");
    }


    /**
     * Gets the {@link Block} that this item is based on
     *
     * @return The {@link Block}
     * @since 0.8
     */
    @Override
    public Block getBlock() {
        return this.block;
    }
}
