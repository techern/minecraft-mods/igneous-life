package org.techern.minecraft.igneouslife.items.tools;

import net.minecraft.block.Block;
import net.minecraft.item.*;
import org.techern.minecraft.igneouslife.items.IgneousItem;
import org.techern.minecraft.igneouslife.items.IgneousLifeItems;

import java.util.List;

/**
 * An extension of {@link AxeItem} that is actually an {@link IgneousItem}
 *
 * @since 0.8
 */
public class IgneousAxe extends AxeItem implements IgneousTool<IgneousAxe> {

    /**
     * The {@link Block} backing this {@link Item}
     *
     * @since 0.8
     */
    private Block block;

    /**
     * Gets the {@link Block} that this item is based on
     *
     * @return The {@link Block}
     * @since 0.8
     */
    public Block getBlock() {
        return block;
    }

    /**
     * Creates a new {@link IgneousAxe} with prefilled data
     *
     * @param blockType The {@link Block} that we are basing this {@link Item} on
     * @since 0.8
     */
    public IgneousAxe(Block blockType) {
        super(ItemTier.STONE, 7.0F, -3.2F, (new Item.Properties()).group(ItemGroup.TOOLS));
        this.block = blockType;

        setRegistryName(blockType.getRegistryName().getPath() + "_axe");
    }

    /**
     * Gets the {@link String} defining the tool type
     *
     * @return a {@link String}, duh
     * @since 0.8
     */
    @Override
    public String getToolType() {
        return "axe";
    }

    /**
     * Gets a {@link List} of all of the {@link IgneousTool}s of this type
     *
     * @return a {@link List}
     * @since 0.8
     */
    @Override
    public List<IgneousAxe> getAll() {
        return IgneousLifeItems.getAxes();
    }
}
