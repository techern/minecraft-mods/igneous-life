package org.techern.minecraft.igneouslife.items;

import net.minecraft.block.Block;

/**
 * An interface for all {@link org.techern.minecraft.igneouslife.IgneousLifeMod} items that are items to extend
 *
 * @since 0.8
 */
public interface IgneousItem {

    /**
     * Gets the {@link Block} that this item is based on
     *
     * @return The {@link Block}
     * @since 0.8
     */
    public Block getBlock();
}
