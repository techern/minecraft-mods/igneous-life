package org.techern.minecraft.igneouslife;

import net.minecraft.block.Block;
import net.minecraft.block.Blocks;
import net.minecraft.data.DataGenerator;
import net.minecraft.item.*;
import net.minecraftforge.client.model.generators.ExistingFileHelper;
import net.minecraftforge.common.MinecraftForge;
import net.minecraftforge.event.RegistryEvent;
import net.minecraftforge.eventbus.api.SubscribeEvent;
import net.minecraftforge.fml.common.Mod;
import net.minecraftforge.fml.event.lifecycle.FMLClientSetupEvent;
import net.minecraftforge.fml.event.lifecycle.FMLCommonSetupEvent;
import net.minecraftforge.fml.event.lifecycle.GatherDataEvent;
import net.minecraftforge.fml.event.server.FMLServerStartingEvent;
import net.minecraftforge.fml.javafmlmod.FMLJavaModLoadingContext;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.techern.minecraft.igneouslife.data.generators.IgneousItemModelProvider;
import org.techern.minecraft.igneouslife.data.generators.IgneousLanguageProvider;
import org.techern.minecraft.igneouslife.data.generators.IgneousRecipeGenerator;
import org.techern.minecraft.igneouslife.data.generators.custom.texture.SwordItemTextureGenerator;
import org.techern.minecraft.igneouslife.data.generators.custom.texture.ToolItemTextureGenerator;
import org.techern.minecraft.igneouslife.items.IgneousLifeItems;
import org.techern.minecraft.igneouslife.items.IgneousSword;
import org.techern.minecraft.igneouslife.items.tools.IgneousAxe;
import org.techern.minecraft.igneouslife.items.tools.IgneousHoe;
import org.techern.minecraft.igneouslife.items.tools.IgneousPickaxe;
import org.techern.minecraft.igneouslife.items.tools.IgneousShovel;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collections;
import java.util.List;

// The value here should match an entry in the META-INF/mods.toml file
@Mod("igneouslife")
public class IgneousLifeMod
{
    // Directly reference a log4j logger.
    private static final Logger LOGGER = LogManager.getLogger();

    public IgneousLifeMod() {
        // Register the setup method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::setup);
        // Register the doClientStuff method for modloading
        FMLJavaModLoadingContext.get().getModEventBus().addListener(this::doClientStuff);

        // Register ourselves for server and other game events we are interested in
        MinecraftForge.EVENT_BUS.register(this);
    }

    private void setup(final FMLCommonSetupEvent event)
    {
        // some preinit code
        LOGGER.info("Hello, cruel world!");
    }

    private void doClientStuff(final FMLClientSetupEvent event) {
        // do something that can only be done on the client
        LOGGER.info("Got game settings {}", event.getMinecraftSupplier().get().gameSettings);
    }
    // You can use SubscribeEvent and let the Event Bus discover methods to call
    @SubscribeEvent
    public void onServerStarting(FMLServerStartingEvent event) {
        // do something when the server starts
        LOGGER.info("HELLO from server starting");
    }



    /**
     * A utility class that handles all {@link RegistryEvent}s
     *
     * @since 0.8
     */
    @Mod.EventBusSubscriber(bus=Mod.EventBusSubscriber.Bus.MOD)
    public static class RegistryEvents {

        /**
         * Registers all new {@link Block} created by this mod
         *
         * @param event The {@link RegistryEvent.Register<Block>} event
         * @since 0.8
         */
        @SubscribeEvent
        public static void onBlocksRegistry(final RegistryEvent.Register<Block> event) {
            // register a new block here

            List<Block> blocks = Arrays.asList(Blocks.GRANITE, Blocks.POLISHED_GRANITE,
                    Blocks.ANDESITE, Blocks.POLISHED_ANDESITE, Blocks.DIORITE, Blocks.POLISHED_DIORITE);

            for (Block block : blocks) {

            }
            LOGGER.info("HELLO from Register Block");
        }

        /**
         * Registers all new {@link Item} created by this mod
         *
         * @param event The {@link RegistryEvent.Register<Item>} event
         * @since 0.8
         */
        @SubscribeEvent
        public static void onItemsRegistry(final RegistryEvent.Register<Item> event) {
            // register a new block here

            List<Block> blocks = Arrays.asList(Blocks.GRANITE, Blocks.POLISHED_GRANITE,
                    Blocks.ANDESITE, Blocks.POLISHED_ANDESITE, Blocks.DIORITE, Blocks.POLISHED_DIORITE);

            for (Block block : blocks) {
                IgneousLifeItems.getPickaxes().add(new IgneousPickaxe(block));
                IgneousLifeItems.getAxes().add(new IgneousAxe(block));
                IgneousLifeItems.getHoes().add(new IgneousHoe(block));
                IgneousLifeItems.getShovels().add(new IgneousShovel(block));

                IgneousLifeItems.getSwords().add(new IgneousSword(block));

            }

            for (Item item : IgneousLifeItems.getAllItems()) {
                event.getRegistry().register(item);
            }
            LOGGER.info("Registered " + IgneousLifeItems.getAllItems().size() + " items");
        }


        /**
         * Called when gathering or generating data
         *
         * @param event The {@link GatherDataEvent}
         * @since 0.8
         */
        @SubscribeEvent
        public static void gatherData(GatherDataEvent event) {

            DataGenerator generator = event.getGenerator();

            generator.addProvider(new IgneousRecipeGenerator(generator));


            if (event.includeClient()) {

                generator.addProvider(new IgneousLanguageProvider(event.getGenerator(), "en_us"));

                generator.addProvider(new ToolItemTextureGenerator<>(generator, new IgneousAxe(Blocks.AIR)));
                generator.addProvider(new ToolItemTextureGenerator<>(generator, new IgneousPickaxe(Blocks.AIR)));
                generator.addProvider(new ToolItemTextureGenerator<>(generator, new IgneousHoe(Blocks.AIR)));
                generator.addProvider(new ToolItemTextureGenerator<>(generator, new IgneousShovel(Blocks.AIR)));

                generator.addProvider(new SwordItemTextureGenerator(generator));

                generator.addProvider(new IgneousItemModelProvider(generator, new ExistingFileHelper(Collections.emptyList(), false)));
                LOGGER.info("Gathered client data");
            }
            if (event.includeServer()) {
                LOGGER.info("Gathered server data");
            }

            try {
                generator.run();
            } catch (IOException e) {
                LOGGER.error("Error generating data" , e);
            }
        }
    }
}
