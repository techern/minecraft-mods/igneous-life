package org.techern.minecraft.igneouslife.data.generators;

import net.minecraft.block.Blocks;
import net.minecraft.data.DataGenerator;
import net.minecraft.data.IFinishedRecipe;
import net.minecraft.data.RecipeProvider;
import net.minecraft.data.ShapedRecipeBuilder;
import net.minecraft.item.Items;
import org.techern.minecraft.igneouslife.items.IgneousLifeItems;
import org.techern.minecraft.igneouslife.items.IgneousSword;
import org.techern.minecraft.igneouslife.items.tools.IgneousAxe;
import org.techern.minecraft.igneouslife.items.tools.IgneousHoe;
import org.techern.minecraft.igneouslife.items.tools.IgneousPickaxe;
import org.techern.minecraft.igneouslife.items.tools.IgneousShovel;

import java.util.function.Consumer;

/**
 * A {@link RecipeProvider} for {@link org.techern.minecraft.igneouslife.items.IgneousItem}s
 *
 * @since 0.9
 */
public class IgneousRecipeGenerator extends RecipeProvider {

    /**
     * Creates a new {@link IgneousRecipeGenerator}
     *
     * @param generatorIn the {@link DataGenerator} we use
     * @since 0.8
     */
    public IgneousRecipeGenerator(DataGenerator generatorIn) {
        super(generatorIn);
    }

    /**
     * Registers all new {@link IFinishedRecipe}s
     *
     * @param consumer The {@link Consumer} consuming the recipes
     * @since 0.8
     */
    @Override
    protected void registerRecipes(Consumer<IFinishedRecipe> consumer) {

        for (IgneousPickaxe pickaxe : IgneousLifeItems.getPickaxes()) {
            ShapedRecipeBuilder.shapedRecipe(pickaxe).key('|', Items.STICK).key('@', pickaxe.getBlock())
                    .patternLine("@@@").patternLine(" | ").patternLine(" | ")
                    .addCriterion("has_" + pickaxe.getBlock().getRegistryName().getPath(), this.hasItem(pickaxe.getBlock()))
                    .build(consumer);
        }

        for (IgneousAxe axe : IgneousLifeItems.getAxes()) {
            ShapedRecipeBuilder.shapedRecipe(axe).key('|', Items.STICK).key('@', axe.getBlock())
                    .patternLine("@@").patternLine("@|").patternLine(" |")
                    .addCriterion("has_" + axe.getBlock().getRegistryName().getPath(), this.hasItem(axe.getBlock()))
                    .build(consumer);
        }

        for (IgneousHoe hoe : IgneousLifeItems.getHoes()) {
            ShapedRecipeBuilder.shapedRecipe(hoe).key('|', Items.STICK).key('@', hoe.getBlock())
                    .patternLine("@@").patternLine(" |").patternLine(" |")
                    .addCriterion("has_" + hoe.getBlock().getRegistryName().getPath(), this.hasItem(hoe.getBlock()))
                    .build(consumer);
        }

        for (IgneousShovel shovel : IgneousLifeItems.getShovels()) {
            ShapedRecipeBuilder.shapedRecipe(shovel).key('|', Items.STICK).key('@', shovel.getBlock())
                    .patternLine("@").patternLine("|").patternLine("|")
                    .addCriterion("has_" + shovel.getBlock().getRegistryName().getPath(), this.hasItem(shovel.getBlock()))
                    .build(consumer);
        }


        for (IgneousSword sword : IgneousLifeItems.getSwords()) {
            ShapedRecipeBuilder.shapedRecipe(sword).key('|', Items.STICK).key('@', sword.getBlock())
                    .patternLine("@").patternLine("@").patternLine("|")
                    .addCriterion("has_" + sword.getBlock().getRegistryName().getPath(), this.hasItem(sword.getBlock()))
                    .build(consumer);
        }

    }


}
