package org.techern.minecraft.igneouslife.data.generators.custom.texture;

import net.minecraft.data.DataGenerator;
import net.minecraft.data.DirectoryCache;
import net.minecraft.data.IDataProvider;
import org.apache.commons.imaging.ImageFormats;
import org.apache.commons.imaging.ImageReadException;
import org.apache.commons.imaging.ImageWriteException;
import org.apache.commons.imaging.Imaging;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;
import org.techern.minecraft.igneouslife.items.tools.IgneousTool;

import java.awt.*;
import java.awt.image.BufferedImage;
import java.io.IOException;
import java.io.OutputStream;
import java.nio.file.*;
import java.util.HashMap;

/**
 * A {@link IDataProvider} for {@link org.techern.minecraft.igneouslife.items.IgneousItem} tool textures
 *
 * @param <T> The type of {@link IgneousTool} being used
 *
 * @since 0.8
 */
public class ToolItemTextureGenerator<T extends IgneousTool<T>> implements IDataProvider {

    /**
     * The {@link DataGenerator} that we will be using
     *
     * @since 0.8
     */
    private final DataGenerator generator;

    /**
     * The {@link Logger} instance we use
     *
     * @since 0.8
     */
    private static final Logger  LOGGER = LogManager.getLogger();

    /**
     * An example tool
     *
     * @since 0.8
     * DO NOT REGISTER THIS TOOL LOL
     */
    private T exampleTool;

    /**
     * Creates a new {@link ToolItemTextureGenerator}
     *
     * @param generator The {@link DataGenerator} to be used
     * @since 0.8
     */
    public ToolItemTextureGenerator(DataGenerator generator, T exampleTool) {
        this.exampleTool = exampleTool;
        this.generator = generator;
    }

    /**
     * Perform the generation and save any data
     *
     * @param cache The {@link DirectoryCache}
     * @throws IOException An exception occurred
     *
     * @since 0.8
     */
    @Override
    public void act(DirectoryCache cache) throws IOException {

        LOGGER.info("Output path: " + generator.getOutputFolder());

        Path blockTexturesDirectory = generator.getOutputFolder().getParent().getParent().resolve("textures_in/blocks/");

        Path inputDataDirectory = generator.getOutputFolder().getParent().getParent().resolve("textures_in/items/" + exampleTool.getToolType() + "s/");

        Path outputTextureFolder = generator.getOutputFolder().resolve("assets/igneouslife/textures/items");

        Files.createDirectories(outputTextureFolder);

        BufferedImage handleImage;

        BufferedImage maskImage;

        //Ok, let's do this!

        try {

            handleImage = Imaging.getBufferedImage(Files.newInputStream(inputDataDirectory.resolve("handle.png")));

            maskImage = Imaging.getBufferedImage(Files.newInputStream(inputDataDirectory.resolve("mask.png")));

        } catch (ImageReadException exception) {
            LOGGER.error("Could not read an image", exception);
            return;
        }



        for (T item : exampleTool.getAll()) {
            String blockName = item.getBlock().getRegistryName().getPath();
            BufferedImage blockImage;

            try {
                blockImage = Imaging.getBufferedImage(Files.newInputStream(blockTexturesDirectory.resolve(blockName + ".png")));
            } catch (ImageReadException exception) {
                LOGGER.error("Could not read block image", exception);
                return;
            }


            BufferedImage finalImage = new BufferedImage(handleImage.getWidth(), handleImage.getHeight(), BufferedImage.TYPE_INT_ARGB);

            for (int xPixel = 0; xPixel < maskImage.getWidth(); xPixel++) {
                for (int yPixel = 0; yPixel < maskImage.getHeight(); yPixel++) {

                    Color handleColor = new Color(handleImage.getRGB(xPixel, yPixel), true);
                    Color finalColor;
                    Color maskColor = new Color(maskImage.getRGB(xPixel, yPixel), true);
                    Color blockColor = new Color(blockImage.getRGB(xPixel, yPixel), true);


                    //Well we know that the handle is 100% needed so we'll overlay the mask and the block
                    if (maskColor.getAlpha() == 0) {
                        finalColor = handleColor;
                        finalImage.setRGB(xPixel, yPixel, finalColor.getRGB());
                    } else {

                        if (maskColor.getRed() < 128) {
                            finalColor = maskColor.getGreen() < 64 ? blockColor.darker().darker() : blockColor.darker();
                        } else {
                            finalColor = blockColor;
                        }

                        if (blockName.toLowerCase().contains("granite")) {
                            finalColor = finalColor.brighter().brighter();
                        }

                        finalImage.setRGB(xPixel, yPixel, finalColor.getRGB());


                    }


                }
            }


            Path filePath = outputTextureFolder.resolve(blockName + "_" + item.getToolType() + ".png");

            LOGGER.info(filePath.toString());

            Files.createFile(filePath);

            LOGGER.info("Can we write? " + Files.isWritable(filePath) + ", " + Files.isWritable(filePath.getParent()));

            try (OutputStream outputStream = Files.newOutputStream(filePath)) {
                Imaging.writeImage(finalImage, outputStream, ImageFormats.PNG, new HashMap<>());
            } catch (ImageWriteException e) {
                LOGGER.error("What the fuck now?!?!?! ", e);
            }


        }

    }

    /**
     * Gets the name of this {@link IDataProvider}
     *
     * @return The name
     * @since 0.8
     */
    @Override
    public String getName() {
        return "ToolItemTextureGenerator";
    }
}
