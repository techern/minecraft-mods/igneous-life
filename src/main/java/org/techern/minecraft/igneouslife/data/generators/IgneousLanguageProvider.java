package org.techern.minecraft.igneouslife.data.generators;

import net.minecraft.data.DataGenerator;
import net.minecraftforge.common.data.LanguageProvider;
import org.techern.minecraft.igneouslife.data.generators.custom.ForgivingLanguageProvider;
import org.techern.minecraft.igneouslife.items.IgneousLifeItems;
import org.techern.minecraft.igneouslife.items.IgneousSword;
import org.techern.minecraft.igneouslife.items.tools.IgneousAxe;
import org.techern.minecraft.igneouslife.items.tools.IgneousHoe;
import org.techern.minecraft.igneouslife.items.tools.IgneousPickaxe;
import org.techern.minecraft.igneouslife.items.tools.IgneousShovel;

/**
 * A {@link LanguageProvider} for {@link org.techern.minecraft.igneouslife.IgneousLifeMod}
 *
 * @since 0.8
 * TODO Investigate creating one per language we want to target
 */
public class IgneousLanguageProvider extends ForgivingLanguageProvider {

    /**
     * Creates a new {@link IgneousLanguageProvider}
     *
     * @param gen    The {@link DataGenerator}
     * @param locale The locale as a {@link String}
     * @since 0.8
     */
    public IgneousLanguageProvider(DataGenerator gen, String locale) {
        super(gen, "igneouslife", locale);
    }

    /**
     * Adds translations to the {@link LanguageProvider}
     *
     * @since 0.8
     */
    @Override
    protected void addTranslations() {

        //First we do all tools
        for (IgneousPickaxe pickaxe : IgneousLifeItems.getPickaxes()) {
            add(pickaxe, pickaxe.getBlock().getNameTextComponent().getString() + " Pickaxe");
        }
        for (IgneousAxe axe : IgneousLifeItems.getAxes()) {
            add(axe, axe.getBlock().getNameTextComponent().getString() + " Axe");
        }
        for (IgneousHoe hoe : IgneousLifeItems.getHoes()) {
            add(hoe, hoe.getBlock().getNameTextComponent().getString() + " Hoe");
        }
        for (IgneousShovel shovel : IgneousLifeItems.getShovels()) {
            add(shovel, shovel.getBlock().getNameTextComponent().getString() + " Shovel");
        }

        for (IgneousSword sword : IgneousLifeItems.getSwords()) {
            add(sword, sword.getBlock().getNameTextComponent().getString() + " Sword");
        }


    }
}
