package org.techern.minecraft.igneouslife.data.generators;

import net.minecraft.data.DataGenerator;
import net.minecraft.item.Item;
import net.minecraftforge.client.model.generators.ExistingFileHelper;
import net.minecraftforge.client.model.generators.ItemModelBuilder;
import net.minecraftforge.client.model.generators.ItemModelProvider;
import net.minecraftforge.client.model.generators.ModelFile;
import org.techern.minecraft.igneouslife.items.IgneousLifeItems;

import java.util.ArrayList;

/**
 * An {@link ItemModelProvider} for all {@link IgneousLifeItems}s
 *
 * @since 0.8
 */
public class IgneousItemModelProvider extends ItemModelProvider {

    /**
     * Creates a new {@link IgneousItemModelProvider}
     *
     * @param generator The {@link DataGenerator} to be used
     * @param existingFileHelper The {@link ExistingFileHelper} helper
     * @since 0.8
     */
    public IgneousItemModelProvider(DataGenerator generator, ExistingFileHelper existingFileHelper) {
        super(generator, "igneouslife", existingFileHelper);
    }

    /**
     * Registers all new models
     *
     * @since 0.8
     */
    @Override
    protected void registerModels() {

        ArrayList<Item> listOfGenerated = new ArrayList<>();

        listOfGenerated.addAll(IgneousLifeItems.getShovels());
        listOfGenerated.addAll(IgneousLifeItems.getHoes());
        listOfGenerated.addAll(IgneousLifeItems.getAxes());
        listOfGenerated.addAll(IgneousLifeItems.getPickaxes());

        listOfGenerated.addAll(IgneousLifeItems.getSwords());

        for (Item item : listOfGenerated) {
            ItemModelBuilder builder = getBuilder(item.getRegistryName().getPath());

            builder.parent(new ModelFile.UncheckedModelFile("item/generated")).texture("layer0", modLoc("items/" + item.getRegistryName().getPath()));

            getBuilder(item.getRegistryName().getPath()).parent(new ModelFile.UncheckedModelFile("item/generated")).texture("layer0", modLoc("items/" + item.getRegistryName().getPath()));
        }

    }

    /**
     * Gets the name of this {@link IgneousItemModelProvider}
     *
     * @return The name
     * @since 0.8
     */
    @Override
    public String getName() {
        return "IgneousItemModelProvider";
    }
}
